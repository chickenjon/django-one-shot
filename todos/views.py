from msilib.schema import ListView
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from todos.models import (
    TodoList,
    TodoItem,
)

# Create your views here.


class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    context_object_name = "list"


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"
    context_object_name = "todo"


class TodoCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", kwargs={"pk": self.object.pk})


class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/update.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", kwargs={"pk": self.object.pk})


class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    context_object_name = "todo"
    success_url = reverse_lazy("todo_list")


class ItemCreate(CreateView):
    model = TodoItem
    template_name = "todos/create_item.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["todolist"] = TodoList.objects.all()

        return context

    def get_success_url(self):
        return reverse_lazy("show_todolist", kwargs={"pk": self.object.list.pk})


class ItemUpdate(UpdateView):
    model = TodoItem
    template_name = "todos/update_item.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["todolist"] = TodoList.objects.all()

        return context

    def get_success_url(self):
        return reverse_lazy("show_todolist", kwargs={"pk": self.object.list.pk})
